/**
 * 1. Напиши функцию createPromise, которая будет возвращать промис
 * */

function createPromise() {}

/**
 * 2. Напиши функцию createResolvedPromise, которая будет возвращать промис, который успешно выполнен (fulfilled)
 * */

function createResolvedPromise() {}

/**
 * 3. Напиши функцию createResolvedPromiseWithData, которая будет возвращать промис, который успешно выполнен
 * (fulfilled) и возвращать при резолве объект вида  { success: true }
 * */

function createResolvedPromiseWithData() {}

/**
 * 4. Напиши функцию createRejectedPromise, которая будет возвращать промис, который будет отклонен (rejected)
 * */

function createRejectedPromise() {}

/**
 * 5. Напиши функцию createRejectedPromiseWithError, которая будет возвращать промис, который будет отклонен
 * (rejected) и отклоненыый промис должен возвращать объект ошибки с текстом "Что-то пошло не так"
 * */

function createRejectedPromiseWithError() {}

/**
 * 6. Напиши функцию fulfilledOrNotFulfilled, которая принимает на вход boolean аргумент и возвращает промис.
 * Если аргумент true, то промис должен быть выполнен, иначе — отклонен.
 * */

function fulfilledOrNotFulfilled(shouldBeResolve) {}

/**
 * 7. Создай функцию timer, которая возвращает промис, который выполняется через 3 секунды.
 * */

function timer() {}

/**
 * 8. Используя async/await, напиши асинхронную функцию. Функция на вход принимает boolean аргумент и передает его в функцию из задания 6.
 * Если функция из задания 6 была зарезолвлена, возвращай 'success'.
 * Если зареджекчена, возвращай 'error'
 */

function callOneAsyncFunction(shouldBeResolve) {}

/**
 * 9. Используя async/await, напиши асинхронную функцию. Функция должна получать результат из функции задания 3 и
 * передавать его в функцию из задания 6. Если функция из задания 6 была зарезолвлена, возвращай 'success'.
 */

function callManyAsyncFunction() {}

module.exports = {
  createPromise,
  createResolvedPromise,
  createResolvedPromiseWithData,
  createRejectedPromise,
  createRejectedPromiseWithError,
  fulfilledOrNotFulfilled,
  timer,
  callOneAsyncFunction,
  callManyAsyncFunction
};
